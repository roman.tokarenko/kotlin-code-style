# Kotlin Code Style

In this repository the set of code conventions about Kotlin code style is presented. It can be updated by the Android team members by creating merge requests containing a case description and code example. So feel free to create MR's into this project. We'll treat them as usual MR's by duscussing/approving/rejecting.

This list of rules enhances these two:
- (https://android.github.io/kotlin-guides/style.html) by Google
- (https://kotlinlang.org/docs/reference/coding-conventions.html) by Kotlin Development Team

It also reconsider some ambiguous moments in them.

# Table of content
1. [Line length](#linelength)
2. [Naming](#naming)
    * 2.1 [Common](#common)
    * 2.2 [Classes and interfaces](#classed_and_interfaces)
3. [Modifier order](#modifier_order)
4. [Expressions formatting](#expressions_formating)
5. [Functions](#function)
    * 5.1 [Single expression functions](#function_expression)
    * 5.2 [Function invocation](#formating_function_calling)
    * 5.3 [Function declaration](#formating_function_declaration)
    * 5.4 [Functional variable invocation](#calling_function_variable)
    * 5.5 [Observable return type](#observable_return_type)
    * 5.6 [Operator "=" and function return type](#operator_equals_and_return_type)
6. [Classes](#classes)
7. [Annotations](#annotation)
8. [Class structure](#class_member_order)
9. [Lambda expressions](#lambda_formating)
10. [Condition operators](#condition_operator)
11. [Template header](#template_header)
12. [Files](#files)


# <a name='linelength'>Line length</a>
- Maximum line length: 120 characters.

# <a name='naming'>Naming</a>
## <a name='common'>Common</a>
- Constant values in the (companion) object should be written in uppercase, with an underscore separating words: `SCREAMING_SNAKE_CASE`
- View fields from Kotlin Extensions should be written using underscore: `lower_snake_case`
- Any other fields should be named like this: `lowerCamelCase`
- Functions should be named like this: `lowerCamelCase`
- Classes should be named like this: `UpperCamelCase`
- Enum values should be named in uppercase with an underscore separating words:
```kotlin
    enum class DialFeedbackType {
        UNKNOWN,
        IN_CALL,
        KEYPAD
    }
```
- Package names are all lower-case, multiple words concatenated together, without hyphens or underscores: `com.oomacorp.funkywidget`
## <a name='classed_and_interfaces'>Classses and interfaces</a>
* Do not use any prefix or suffix in an interface name.
Try to name an interface according to its semantics and responsibility, for example: Formatter, ResourceProvider, LocationProvider, Spannable, Resizeable.
* Name implementation according to its implementations details, for example: AccountFormatter, MoneyFormatter, GpsLocationProvider, GeoIpLocationProvider  
* For rare case when we need to separate an interface and an implementation do not use any prefix or suffix for interface name and use suffix `Impl` for implementation:
```kotlin
    interface ContactsInteractor
    
    class ContactsInteractorImpl : ContactsInteractor {
        // methods implementation
    }
```

# <a name='modifier_order'>Modifier order</a>
1) override
2) public / protected / private / internal
3) final / open / abstract
4) const / lateinit
5) inner
6) enum / annotation / sealed / data
7) companion
8) inline
9) infix
10) operator

# <a name='expressions_formating'>Expressions formatting</a>

While moving the part of method calls chain on the new line `.` character or `?.` operator should be moved to the new line, in case of using property calls chain it is allowed to leave it on the same line:
```kotlin
val collectionItem = source.collectionItems
                ?.dropLast(10)
                ?.sortedBy { it.progress }
```
Elvis operator `?:` while tearing the expression apart should be moved to the new line:
```kotlin
val promoItemDistanceTradeLink: String = promoItem.distanceTradeLinks?.appLink
            ?: String.EMPTY
```
While declaring a val with a delegate that is too long and can't be placed on a single line, leave opening curly brace on the current line and move the rest of an expression to the next line:
```kotlin
private val promoItem: MarkPromoItem by lazy {
        extractNotNull(BUNDLE_FEED_UNIT_KEY) as MarkPromoItem
}
```

# <a name='function'>Functions</a>
## <a name='function_expression'>Single expression functions</a>
* It is allowed to use a single expression function (aka a function with a body expression) only if it can be placed within a single line.

## <a name='formating_function_calling'>Function invocation</a>
* Using Kotlin named parameters is at the discretion of a developer. Need to consider the complexity of a method. If a method invocation is simple and concsice, there is no need to use named parameters.
While using named parameters each parameter should be on the new line with standard indent and closing paranthesis should be also on the new line:

```kotlin
runOperation(
	method = operation::run,
	consumer = consumer,
	errorHandler = errorHandler,
	tag = tag,
	cache = cache,
	cacheMode = cacheMode
)
```

## <a name='formating_function_declaration'>Function declaration</a>

* While breaking the line with function declaration move every parameter of a function to the new line with an indent and move the closing paranthesis to the new line too.

## <a name='calling_function_variable'>Functional variable invocation</a>

* Always use comprehensive option by using `invoke()` after functional variable instead of contracted option which uses just paranthesis `()`:
```kotlin
fun runAndCall(expression: () -> Unit): Result {
        val result = run()

        //Bad
        expression()
        //Good
        expression.invoke()

        return result
}
```

## <a name='observable_return_type'>Methods returning Observable/Flowable naming rule</a>
* Methods which include return type Observable/Flowable should include it in naming
```kotlin
fun getPersonsObservable(): Observable<Person> =
    personsSubject
```

## <a name='operator_equals_and_return_type'>Operator "=" and function return type</a>
* Operator "=" must be used in functions along with declaring a return type of a function

Bad
```kotlin
fun createPersonsData() =
    listOf(Person())
```
Good
```kotlin
fun createPersonsData(): List<Person> =
    listOf(Person())
```
* Don't use expression body if function return type is Unit. Use block body for such cases.

Bad
```kotlin
fun clearPersonsData() =
    someList.clear()
```
Good
```kotlin
fun clearPersonsData() {
    someList.clear()
}
```

# <a name='classes'>Classes</a>
- While breaking the line move every class parameter on the new line with a double indent and and move the closing paranthesis to the new line too.
```kotlin
data class CategoryStatistic(
        val id: String,
        val title: String,
        val imageUrl: String,
        val percent: Double
) : Serializable
```
- Classes with longer headers should be formatted so that each primary constructor parameter is in a separate line with indentation. Also, the closing parenthesis should be on a new line. If we use inheritance, then the superclass constructor call or list of implemented interfaces should be located on the same line as the parenthesis.
- For classes with a long supertype list and this list couldn't be placed within a single line, put a line break after the colon and align all supertype names horizontally
- For multiple interfaces, the superclass constructor call should be located first and then each interface should be located in a different line.
- Using named constructor parameters is at the discretion of a developer. It is needed to consider the complexity of a constructor. If the constructor invocation is simple and concsice, there is no need to use named parameters.

# <a name='annotation'>Annotations</a>
- Annotations should be placed above the class/field/method declaration
- If class/field/method has several annotations they should be placed on the new line each:
```kotlin
@JsonValue
@JvmField
var promoItem: PromoItem? = null
```
- If a property/method has only one annotaion without params it should be placed above it.
- Annotations that are applied to a whole file should be placed stright after comment to the file before the `package`, and it should be followed by the line break.

# <a name='class_member_order'>Class structure</a>
1) Companion object
2) Fields: abstract, override, public, internal, protected, private
3) Initialisation block: init, constructors
4) Abstract methods
5) Overriden base class methods in the same order that in the base class
6) Implementation interface methods in the same order as in the interface
7) public methods
8) internal methods
9) protected methods
10) private methods
11) inner classes

# <a name='lambda_formating'>Lambda expressions</a>

- If lambda expression is short and can be placed within a single line use `it` as an argument.
- While using lambda as a parameter of a method move it out from the paranthesis if it is the only parameter or last parameter.
- If it is possible to write an expression using function reference, use this way
```kotlin
viewPager.adapter = QuestAdapter(quest, this::onQuestClicked)
```
- don't use `it` as an argument for nested lambda expressions. Use short semantic names instead.
- If a lambda expression couldn't be placed within a single line always use named paramater instead of `it`:
```kotlin
viewPager.adapter = QuestAdapter(quest, { quest ->
        onQuestClicked(quest)
})
```
- Replace unused lambda expression parameters by character `_`.
- Avoid using [Destructuring Declarations](https://kotlinlang.org/docs/reference/multi-declarations.html) in lambda expressions.

# <a name='condition_operator'>Conditional operators</a>
Do not wrap `if` expression into curly braces only if the `if` expression can be placed in a single line. Wrap in curly braces in all other cases.
Use conditional operators as expressions if you can:
```kotlin
return if (condition) foo() else bar()
```
While using `when` operator for short expressions under conditions place them on a single line without curly braces. Use `,` character for two or more values by which a certain expression should be performed.
```kotlin
when (someCondition) {
        0, 1 -> fooFunction()
        2 -> barFunction()
        else -> exitFunction()
}
```
If curly braces are used at least in one branch, use curly braces for all branches.
In case of multi-line expressions under `when` operator use curly braces for such blocks and separate them from each other by line break:
```kotlin
when (feed.type) {
        FeedType.PERSONAL -> {
        	with(feed as PersonalFeed) {
        	        datePopupStart = dateBegin
        		datePopupEnd = dateEnd
        	}
        }

        FeedType.SUM -> {
        	with(feed as SumFeed) {
        		datePopupStart = dateBegin
        		datePopupEnd = dateEnd
        	}
        }

        FeedType.CARD -> {
        	with(feed as CardFeed) {
        		datePopupStart = dateBegin
        		datePopupEnd = dateEnd
        	}
        }

        else -> {
        	Feed.EMPTY
        }
}
```

# <a name='template_header'>Template header</a>

- Do not use Template Header for classes (it is about author info and creation date).

# <a name='files'>Files</a>

- It is allowed to write several classes in one file only for `sealed` classes. It other cases use one class — one file principle. Exceptions are `inner` classes only.
